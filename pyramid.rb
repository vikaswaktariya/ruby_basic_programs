#1. Write a program that prints a star pyramid.
puts "Enter height of pyramid"
height = gets.to_i

for i in 1..height do
  star = 0
  for j in 1..height - i do
    print " "
  end 

  while star != 2 * i - 1
    print "*"
    star += 1    
  end
  print "\n"
end
 
