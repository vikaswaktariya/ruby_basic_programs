#3. Using hash table, print the frequency of occurence of each character inside an array.
 array_element = [] 

 puts "How many element you want to insert in array"
 no_of_element = gets.to_i

 puts "Enter array character"
 (0..(no_of_element - 1)).each do |index|
   array_element[index] = gets.chomp.to_s  
 end	

 hash = Hash.new(0)
 array_element.each{|key| hash[key] += 1}
 puts "Frequency of each array character: ", hash
