#8.Read from a CSV file, multiple two columns, and then write back to the CSV file.
require 'csv'
  
  CSV.foreach("myFile.csv") do |row|
    print row, "\n"
  end

  CSV.open("myFile.csv", "wb") do |csv|
    csv << ["11", "12"]
    csv << ["21", "22"]
    csv << ["31", "32"]
  end
    
    
