#4. Join 2 arrays without using inbuilt functions.
 array1_element = []
 array2_element = []

 puts "How many element you want to insert in array 1:\n"
 array1_no_of_element = gets.to_i

 puts "Enter 1st array element"
 (0..(array1_no_of_element - 1)).each do |index|
   array1_element[index] = gets.chomp
 end 

 puts "How many element you want to insert in array 1:\n"
 array2_no_of_element = gets.to_i

 puts "Enter 2nd array element"
 (0..(array2_no_of_element - 1)).each do |index|
   array2_element[index] = gets.chomp
 end

 puts "After join 2 array :", array1_element + array2_element 
